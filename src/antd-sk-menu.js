
import { SkMenuImpl } from "../../sk-menu/src/impl/sk-menu-impl.js";

export class AntdSkMenu extends SkMenuImpl {

    get prefix() {
        return 'antd';
    }

}
